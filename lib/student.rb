class Student
  attr_reader :first_name, :last_name, :courses
  def initialize(first, last)
    @first_name = first
    @last_name = last
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(course)
    @courses.each { |x| raise "Conflicts w/ #{x.name}" if x.conflicts_with?(course) }
    @courses << course unless @courses.include? course
    course.students << self unless course.students.include? self
  end

  def course_load
    course_hash = Hash.new(0)
    @courses.each { |x| course_hash[x.department] += x.credits }
    course_hash
  end

end
